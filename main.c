#include <stdio.h>
#include <stdlib.h>
#define ARRAY_LEN 512

int sort(int count, int *array);

//File Reader Ref: https://stackoverflow.com/questions/3501338/c-read-file-line-by-line
int main(int args, char** argv)
{
  printf("File name is: %s\n", argv[1]);
  int i, *array, *sorted_array, array_size;
  array = malloc(ARRAY_LEN * sizeof(int*));

  FILE * fp;
  char * line = NULL;
  size_t len  = 0;
  ssize_t read;

  //Open the file that its name comes from command line
  fp = fopen(argv[1], "r");
  if (fp == NULL)
      exit(EXIT_FAILURE);

  //File reading line by line
  i = 0;
  while ((read = getline(&line, &len, fp)) != -1)
  {
      *(array + i) = atoi(line);
      printf("%d\n", array[i]);
      i++;
  }
  array_size = i - 1;
  // (sort(array_size, array));
  int *a;
  a = (sort(array_size, array));
  printf("\nC DEBUG %d\n", a[0]);
  // printf("\n %d \n", a[6]);  
  // int max = sort(array_size, array);
  // printf("Max: %d\n", max);
  /*

  sorted_array = malloc(array_size * sizeof(int*));

  sorted_array = sort(array_size, array);

  for (i = 0; i < array_size - 1; i++)
  {
    printf("%d , ", *(sorted_array + i));
  }
  printf("%d\n", *(sorted_array + i));
  */

  return 0;
}
