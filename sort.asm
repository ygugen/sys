segment .data
break: db "*******************", 10, 0

segment .text
global sort
extern printf
extern malloc

sort:
    push ebp
    mov ebp, esp
    sub esp, 4            ;max element of array

    mov edx, [ebp + 8]    ;first  arg: len
    mov ecx, [ebp + 12]   ;second arg: arr
    mov eax, [ecx]        ; eax is arr[0]

loop:
    add ecx, 4
    cmp eax, [ecx]
    jl swap
inside:
    dec edx
    cmp edx, 1
    jg loop
    jmp end_max
swap:
    mov eax, [ecx]
    jmp inside
end_max:
    mov [ebp - 4], eax    ;max = [ebp - 4]
;---------------end_of_max--------------------
    
    xor edx, edx          ;edx => sayac = 0

    mov eax, [ebp - 4]    ;eax = max
    imul eax, 4           ;-First element of indexing array
    add eax, 8            ; should be (max * 4 + 8)
    mov ecx, ebp          ;ecx = ebp
    sub ecx, eax          ;ecx = ecx - eax => first element address of array
    ;inc edx               ;edx = 1 (sayac)
    xor esi, esi
zero_loop:
    mov dword [ecx], 0        ;array[i] = 0
    inc esi               ;esi++     (counter)
    add ecx, 4            ;ecx += 4  (array address)
    cmp esi, [ebp - 4]
    jl zero_loop
;-------------end_of_zero_array-------------- 2. arrayin icerigi sifirlaniyor
    mov edx, [ebp + 8]    ;edx = len of input_array
    mov ecx, [ebp + 12]   ;ecx = array address
    xor eax, eax          ; int i = 0, (eax = i)
counting_loop:
    mov ebx, eax          ; (i*4)+ecx = arr[i]
    imul ebx, 4
    add ebx, ecx          ; [ebx] = arr[i]
    mov ebx, [ebx]
    mov esi, [ebp-4]
    sub esi, ebx
    mov ebx, esi
    imul ebx, 4
    add ebx, 8
    mov esi, ebp
    sub esi, ebx
    mov ebx, esi
    mov esi, [ebx]
    inc esi
    mov [ebx], esi
    inc eax               ; i++ (eax++)
    cmp eax, edx          ; if(i< arr.length) (eax < edx)
    jl counting_loop

;-----------end_of_count_occurrences------------

_malloc:
    mov ecx, [ebp+8]
    imul ecx, 4
    push ecx                ; push amount of bytes malloc should allocate    
    ;
    call malloc           ; call malloc
    test eax, eax          ; check if the malloc failed
    add esp,4              ; undo push
    
    xor esi, esi ; esi = i
    xor edi, edi ; edi = j

reconstruct:
    jmp while
inw:
    ; Execute the content of the loop
    ; eax is adress of collection array
    mov ecx, edi
    imul ecx, 4
    add eax, ecx
    mov eax, edi
    
    inc edi         ; j++
    
    mov ebx, [ebp-4] ; {
    sub ebx, esi
    imul ebx, 4
    sub ebx, 8
    mov ecx, ebp
    sub ecx, ebx  ; ecx is adress of countarr[i]  }
    
    

    mov edx, [ecx]  ; countarr[i]-- {
    dec edx
    mov [ecx], edx  ;}
    
    

while:
    mov ebx, [ebp-4] ; {
    sub ebx, esi
    imul ebx, 4
    sub ebx, 8
    mov ecx, ebp
    sub ecx, ebx  ; ecx is adress of countarr[i]  }

    mov ecx, [ecx]
    cmp ecx,0   ; Check the condition
    jg inw   ; Jump to content of the loop if met

    inc esi      ; Increment
    cmp esi,[ebp-4]    ; Compare esi to the limit
    jle reconstruct   ; Loop while less or equal
donerecon:
%if 0
%endif
    
    mov esp, ebp
    pop ebp
    ret